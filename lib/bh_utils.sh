# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# A set of functions that I find useful to have around when creating bash     #
# scripts. Some of the functions required that bh_init.sh is called first,    #
# these are indicated in their docstrings                                     #
#                                                                             #
# This should be called prior to calling set -u as it initialised variables   #
# that may be empty.                                                          #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

# Initialise verbosity
if [[ -z $VERBOSE ]]; then
    VERBOSE=1
fi

# Should info messages to sent to STDERR, if not set then this is initialised
# to no
if [[ -z $VERBOSE_TO_STDERR ]]; then
    VERBOSE_TO_STDERR=1
fi


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Strip the first row from a file and pass it through to the command
# following the call to body.
#
# Globals:
#   None
# Arguments:
#   The command call. For example to sort a file with a header line and keep
#   the header at the top: 'cat <file> | body sort'
# Outputs:
#   Whatever the command outputs.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
body() {
    IFS= read -r header
    printf '%s\n' "$header"
    "$@"
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Strip the first row from a file and pass the remainder of the file through to
# the command following the call to strip.
#
# Globals:
#   None
# Arguments:
#   The command call. For example to sort a file with a header line and remove
#   the header at the header to prevent it being sorted:
#   'cat <file> | strip sort'
# Outputs:
#   Whatever the command outputs, minus the file header.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
strip() {
    IFS= read -r header
    "$@"
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Locate the system tmp directory value. This works by creating a tmp file
# with system defaults and finding the dirname.
#
# Globals:
#   None
# Arguments:
#   None
# Outputs:
#   The system tmp directory location.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
find_tmp() {
    local test_file="$(mktemp)"
    local tmp_dir="$(dirname "$test_file")"
    rm "$test_file"
    echo "$tmp_dir"/
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# The VERBOSE global variable is used in a lot of functions so I make sure it
# is correctly initialised as set -e is often on. This function performs that
# task. If it is not initialised then it is initialised to 1 (False). If
# set -e is active this will temporarily switch it off before the VERBOSE
# check and then reset the state after.
#
# Globals:
#   None
# Arguments:
#   None
# Outputs:
#   None but it does set a global VERBOSE variable (if needed)
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
init_verbose() {
    # If set -e is on I need to switch off while I check for VERBOSE being
    # initialised
    local has_e=${-//[^e]/}
    if [[ "$has_e" == 'e' ]]; then
        set +e
    fi

    # If VERBOSE is not initialised then initialise to False (1)
    if [[ -z "$VERBOSE" ]]; then
        VERBOSE=1
    fi

    # Switch back to the state we started in
    if [[ "$has_e" == 'e' ]]; then
        set -e
    fi
}

# Here we ensure that VERBOSE is properly initialised
init_verbose


###############################################################################
# These are also in bh_init but are included here just in case it it not used #
###############################################################################
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
error_msg() {
    # Output an error message
    local msg="[error] ""$1"

    # We output to STDERR and STDOUT
    echo "$msg" 1>&2
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
info_msg() {
    if [[ $VERBOSE -eq 0 ]]; then
        # Output an error message
        local msg="[info] ""$1"

        if [[ $VERBOSE_TO_STDERR -eq 0 ]]; then
            # We output to STDERR
            echo "$msg" 1>&2
        else
            echo "$msg"
        fi
    fi
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# When passed a directory path, this will create it (using mkdir -p) and set
# it to a global OUTDIR variable. It will also echo out the location of the
# output directory (if VERBOSE=0). This required that info_msg is available
# (i.e bh_init.sh has been called).
#
# Globals:
#   None
# Arguments:
#   $1: The name of the output directory you want set
# Outputs:
#   None but it does set a global OUTDIR variable
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set_global_outdir() {
    if [ $# -eq 0 ]; then
        echo 'error: outdir is missing' >&2
        flags_help
        exit 1
    fi
    OUTDIR=$1
    mkdir -p "$OUTDIR"
    info_msg "output directory: $OUTDIR"
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Return the 1-based position of a string in an array
#
# Globals:
#   None
# Arguments:
#   $1: The name of what we want to find in the array
#   $@: The array we want to check
# Outputs:
#   The 1-based index position of $1 in the array, or -1 if it is not found
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
get_index_one() {
    local col_name="$1"
    shift
    local columns=("$@")

    local idx="1"
    local colno="-1"
    for i in "${columns[@]}"; do
        if [[ "$i" == "$col_name" ]]; then
            colno="$idx"
            break
        fi
        idx=$(( idx + 1 ))
    done
    echo "$colno"
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Return the 0-based position of a string in an array
#
# Globals:
#   None
# Arguments:
#   $1: The name of what we want to find in the array
#   $@: The array we want to check
# Outputs:
#   The 0-based index position of $1 in the array, or -1 if it is not found
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
get_index_zero() {
    local col_name="$1"
    shift
    local columns=("$@")

    local idx="0"
    local colno="-1"
    for i in "${columns[@]}"; do
        if [[ "$i" == "$col_name" ]]; then
            colno="$idx"
            break
        fi
        idx=$(( idx + 1 ))
    done
    echo "$colno"
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Given an array of integers this returns the rank order of the integers in
# the same order that they were passed to the function. I would only use this
# so small sets. I mainly use this to determine column index after cutting
# columns from a file.
#
# Globals:
#   None
# Arguments:
#   $@: An integer array
# Outputs:
#   $@: An integer array but with the rank order of the integers that were
#       passed in
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
rank_order() {
    local integers=("$@")

    # This is a bit nasty but I could not think of any other way to do it off
    # the top of my head
    echo ${integers[@]} |
        tr ' ' '\n' |
        awk '{print $1,NR}' |
        sort -n -k1,1 |
        awk '{print $0, NR}' |
        sort -k2,2 |
        cut -d' ' -f3 |
        tr '\n' ' '
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Set the sort parameters in an array. This sets up "optional" sort arguments
# such as `--compressed`, `--parallel`, `--buffer-size` and `--tmp-dir` (not
# strictly an argument to sort). These are performance options for sort and
# might vary depending on the user requirements.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set_sort_args() {
    # Initialise the global, in bash < 4.4 set -u will kill this if it is empty
    SORT_ARGS=()

    # If set -e is on I need to switch off while I check for cmd-line args
    # being set
    has_u=${-//[^u]/}
    if [[ "$has_u" == 'u' ]]; then
        set +u
    fi

    if [[ -d "$FLAGS_tmp_dir" ]]; then
        FLAGS_tmp_dir="${FLAGS_tmp_dir%/}"
        SORT_ARGS+=('-T' "$FLAGS_tmp_dir"/)
    else
        # Initialise to system /tmp to prevent set -u killing the
        # empty array
        SORT_ARGS+=('-T' "$(find_tmp)")
    fi

    if [[ ! -z "$FLAGS_parallel" ]] && [[ "$FLAGS_parallel" -gt 1 ]]; then
        SORT_ARGS+=('--parallel' "$FLAGS_parallel")
    fi

    if [[ ! -z "$FLAGS_buffer_size" ]] && [[ "$FLAGS_buffer_size" -gt 0 ]]; then
        SORT_ARGS+=('-S' "$FLAGS_buffer_size")
    fi

    if [[ "$has_u" == 'u' ]]; then
        set -u
    fi
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# set the verbose args for a program. This assumes that the verbose flag is a
# boolean -v flag. This function will check VERBOSE and if it is set to 0
# (True) then it will set a global VERBOSE_ARG variable to `-v` otherwise it
# will be an empty string
#
# Globals:
#   VERBOSE
# Arguments:
#   None
# Outputs:
#  None, but will set VERBOSE_ARGS
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set_verbose_arg() {
    VERBOSE_ARG=""

    if [[ $VERBOSE -eq 0 ]]; then
        VERBOSE_ARG="-v"
    fi
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Set the location of info messages to STDERR instead of STDOUT. There is
# probably a better more bash like way of doing this but this will do for now.
#
# Globals:
#   VERBOSE_TO_STDERR
# Arguments:
#   None
# Outputs:
#  None, but will set VERBOSE_TO_STDERR to 1 (True)
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set_verbose_stderr() {
    VERBOSE_TO_STDERR=0
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Check that the number of positional arguments matches up to what is expected
#
# Globals:
#   None
# Arguments:
#   $1: Number of expected arguments (integer)
#   $@: An array of positional command line arguments
# Returns:
#   error_state: 1 if the positionals do not match 0 if they do. This will be
#                captured by $? and fire an error trap if 1
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
check_positional_args() {
    local expected="$1"
    shift
    local args=("$@")

    if [[ ${#args[@]} -ne $expected ]]; then
        error_msg "positional args expected '$expected' got '${#args[@]}'"
        return 1
    fi
    return 0
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Check that the number of positional arguments is at least certain number.
#
# Globals:
#   None
# Arguments:
#   $1: Number of expected arguments (integer)
#   $@: An array of positional command line arguments
# Returns:
#   error_state: 1 if the positionals do not match 0 if they do. This will be
#                captured by $? and fire an error trap if 1
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
check_at_least_positional_args() {
    local at_least="$1"
    shift
    local args=("$@")

    if [[ ${#args[@]} -lt $at_least ]]; then
        error_msg "positional args expected at least '$at_least' got '${#args[@]}'"
        return 1
    fi
    return 0
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Check that the given file is readable. If not an error message is issued
#
# Globals:
#   None
# Arguments:
#   $1: The file to check for readality
#   $2: An optional error message to display if not readable, a default is
#       used if not supplied
# Returns:
#   error_state: 1 if not readable, 0 if readable. This will be captured by $?
#                and fire an error trap if 1
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
check_file_readable() {
    local check_file="$1"
    local default_msg="file not readable: $check_file"
    local msg="${2:-$default_msg}"

    if [[ ! -r "$check_file" ]]; then
        error_msg "$msg"
        return 1
    fi
    return 0
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Check that the directory containing (or will contain) the given file is
# writable. If not an error message is issued
#
# Globals:
#   None
# Arguments:
#   $1: The file to check for writability
#   $2: An optional error message to display if not writable, a default is
#       used if not supplied
# Returns:
#   error_state: 1 if not writable, 0 if writable. This will be captured by $?
#                and fire an error trap if 1
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
check_file_writable() {
    local check_file="$1"
    local default_msg="file not writable: $check_file"
    local msg="${2:-$default_msg}"

    if [[ ! -w "$(dirname "$check_file")" ]]; then
        error_msg "$msg"
        return 1
    fi
    return 0
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Check that the given dir is readable. If not an error message is issued.
# This will also issue an error message and return 1 if $1 is not a directory
#
# Globals:
#   None
# Arguments:
#   $1: The dir to check for readality
#   $2: An optional error message to display if not readable, a default is
#       used if not supplied
# Returns:
#   error_state: 1 if not readable, 0 if readable. This will be captured by $?
#                and fire an error trap if 1
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
check_dir_readable() {
    local check_dir="$1"
    local default_msg="dir not readable: $check_dir"
    local msg="${2:-$default_msg}"

    if [[ ! -d "$check_dir" ]]; then
        error_msg "not a directory: $check_dir"
        return 1
    fi

    if [[ ! -r "$check_dir" ]]; then
        error_msg "$msg"
        return 1
    fi
    return 0
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Check that the directory is writable. If not an error message is issued.
# This will also issue an error message and return 1 if $1 is not a directory
#
# Globals:
#   None
# Arguments:
#   $1: The dir to check for writability
#   $2: An optional error message to display if not writable, a default is
#       used if not supplied
# Returns:
#   error_state: 1 if not writable, 0 if writable. This will be captured by $?
#                and fire an error trap if 1
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
check_dir_writable() {
    local check_dir="$1"
    local default_msg="dir not writable: $check_dir"
    local msg="${2:-$default_msg}"

    if [[ ! -d "$check_dir" ]]; then
        error_msg "not a directory: $check_dir"
        return 1
    fi

    if [[ ! -w "$check_dir" ]]; then
        error_msg "$msg"
        return 1
    fi
    return 0
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Check that the value is positve (> 0). If not an error message is issued.
#
# Globals:
#   None
# Arguments:
#   $1: The value to check for positivity
#   $2: An optional error message to display if <=0, a default is
#       used if not supplied
# Returns:
#   error_state: 1 if <= 0, 0 > 0. This will be captured by $? and fire an
#                error trap if 1
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
check_positive() {
    local value="$1"
    local default_msg="value not positive: $value"
    local msg="${2:-$default_msg}"

    if [[ $value -le 0 ]]; then
        error_msg "$msg"
        return 1
    fi

    return 0
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Turn a bash array into a delimited string.
#
# Globals:
#   None
# Arguments:
#   $1: The delimiter to use on the string.
#   $@: One or more arguments to make into a delimited string.
# Returns:
#   Nothing
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
make_delimited_str() {
    local delimiter="$1"
    shift
    local args=("$@")
    echo "${args[@]}" | tr ' ' "$delimiter"
}
