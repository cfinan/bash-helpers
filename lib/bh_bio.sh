# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# Not much in here but a could of functions that are useful for either passing #
# through VCF headers or stripping them on the fly                             #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

# Some useful constants
VCF_CHR_NAME=1
VCF_START_POS=2
VCF_VAR_ID=3
VCF_REF_ALLELE=4
VCF_ALT_ALLELES=5
VCF_QUALITY=6
VCF_FILTER=7
VCF_INFO=8
VCF_FORMAT=9

HUMAN_AUTOSOMES=(
    1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22
)

HUMAN_SEX_CHROMOSOMES=(
    X Y
)

HUMAN_EXTRA_CHROMOSOMES=(
    MT
)

VCF_BASE_HEADER=(
    "#CHROM"
    "POS"
    "ID"
    "REF"
    "ALT"
    "QUAL"
    "FILTER"
    "INFO"
)

VCF_SAMPLE_HEADER=(
    ${VCF_BASE_HEADER[@]}
    "FORMAT"
)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# set a global array (bcf_out_args) containing the bcftools output arguments
# based on what has been input
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
bcftools_out_args() {
    local output="$1"
    local output_type="$2"
    local use_tmp="${3:-1}"

    bcf_out_args=()
    if [[ "$output" != '-' ]]; then
        local output="$(readlink -f "$output")"
        check_file_writable "$output" \
                            "can't write to bcftools output file location"

        if [[ "$use_tmp" != "1" ]]; then
            check_file_writable "$use_tmp"
            bcf_out_args+=(-o "$tmp_out")
        else
            bcf_out_args+=(-o "$output")
        fi
    fi

    if [[ "$output_type" =~ ^[zuvb]$ ]]; then
        bcf_out_args+=(-O "$output_type")
    else
        error_msg "unknown bcftools output type: $output_type"
        return 1
    fi
    return 0
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# set a global array (bcf_sort_args) containing the bcftools output arguments
# based on what has been input
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
bcftools_sort_args() {
    local tmpdir="$1"
    local sortmem="${2:-0}"

    bcf_sort_args=()
    if [[ "$sortmem" -ne 0 ]]; then
        bcf_sort_args+=(-m "$sortmem")
    fi

    # The tmpdir should always be supplied so we check that it is writable
    check_dir_writable "$tmpdir" "bcftools tmp not writable"

    # Now as a safety feature we create a subdir in the temp dir for bcftools
    # to use as it does not seem to guarentee unique tmpfile names
    # (when merging)
    local bcftools_temp="$(mktemp -d -p"$tmpdir")"
    bcf_sort_args+=(-T "$bcftools_temp")
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
has_fasta_index() {
    local infile="$1"

    if [[ ! -e "$infile".fai ]]; then
        error_msg "no .fai file found"
        return 1
    elif [[ ! -e "$infile".gzi ]]; then
        error_msg "no .gzi file found"
        return 1
    fi
    return 0
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Generate a file containing a VCF header derived from the input file header
# but with the contigs placed in natural sort order. This requires the VCF to
# be tabix indexed. If it is not a tabix index will be created in TMPDIR and
# used to derive the chromosomes
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
make_str_sort_header() {
    local invcf="$1"
    local out_header="$2"
    local reference_genome="${3:-""}"

    # If a reference genome file has been provided, we use the contigs
    # listed in that to build the new header around
    local chroms=()
    if [[ -e "$reference_genome" ]]; then
        has_fasta_index "$reference_genome"
        local chroms=($(cut -f1 "$reference_genome".fai | sort -u))
    elif [[ -e "$invcf".tbi ]]; then
        local chroms=($(tabix --list-chroms "$invcf" | sort))
    else
        # The fall back is a direct extraction from the VCF file
        local chroms=(
            # All the chromosomes, unique in natural sort order
            $(zcat "$invcf" | cut -f1 | sort -u | grep -v -P "^#" | sort)
        )
    fi
    contig_out=1
    bcftools view -h "$invcf" | while IFS= read -r line
    do
        if [[ contig_out -eq 1 ]] && [[ "$line" =~ ^##contig ]]; then
            # output the contigs
            for i in "${chroms[@]}"; do
                echo "##contig=<ID=${i}>" >> "$out_header"
            done
            contig_out=0
            continue
        elif [[ contig_out -eq 1 ]] && [[ "$line" =~ ^##INFO ]]; then
            # output the contigs
            for i in "${chroms[@]}"; do
                echo "##contig=<ID=${i}>" >> "$out_header"
            done
            contig_out=0
        elif [[ "$line" =~ ^##contig ]]; then
            continue
        fi
        echo "$line" >> "$out_header"
    done
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# This passes the VCF header over a piped call but it is still output to
# STDOUT the other side
# so the command below will output the vcf header and the first 10 variants
# from the vcf file no matter how long the vcf header is. Note there is no pipe
# between `vcf_header_pass` and `head`:
# `zcat vcf_file.vcf.gz | vcf_header_pass head -n10`
#
# # Globals:
#   None
# Arguments:
#   $@: A command call with arguments
# Outputs:
#   vcf_header: The VCF header
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
vcf_header_pass() {
    while true; do
	      IFS= read -r header

	      if [[ "$header" =~ ^## ]]; then
	          printf '%s\n' "$header"
	          continue
	      elif [[ "$header" =~ ^#CHROM ]]; then
	          printf '%s\n' "$header"
	          break
	      else
	          echo "not a vcf!" 1>&2
	      fi
    done
    "$@"
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# This strips the VCF header before a piped call but leaves the `#CHROM` row
# (sample header) the remainder of the vcf file is output to the command.
# So the command below will output the first 10 variants from the vcf file no
# matter how long the vcf header is. Note there is no pipe between
# `vcf_header_pass` and `head`:
# `zcat vcf_file.vcf.gz | vcf_header_pass head -n10`
#
# # Globals:
#   None
# Arguments:
#   $@: A command call with arguments
# Outputs:
#   header: The sample header
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
vcf_header_strip() {
    while true; do
	      IFS= read -r header

	      if [[ "$header" =~ ^## ]]; then
	          continue
	      elif [[ "$header" =~ ^#CHROM ]]; then
	          printf '%s\n' "$header"
            # echo "$header"
	          break
	      else
	          echo "not a vcf!" 1>&2
	      fi
    done
    "$@"
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# This strips the full VCF header before a piped call, including the `#CHROM`
# row (sample header) the remainder of the vcf file is output to the command.
# So the command below will output the first 10 variants from the vcf file no
# matter how long the vcf header is. Note there is no pipe between
# `vcf_header_pass` and `head`:
# `zcat vcf_file.vcf.gz | vcf_header_pass head -n10`
#
# # Globals:
#   None
# Arguments:
#   $@: A command call with arguments
# Outputs:
#   header: The sample header
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
vcf_header_full_strip() {
    while true; do
	      IFS= read -r header

	      if [[ "$header" =~ ^## ]]; then
	          continue
	      elif [[ "$header" =~ ^#CHROM ]]; then
	          break
	      else
	          echo "not a vcf!" 1>&2
	      fi
    done
    "$@"
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Make sure the chromosome string input is valid. This is designed to process
# chromosome names from the command line given as a comma separated string and
# validate them against a set of valid chromosome names. This requires
# bh_utls.sh to be sourced in.
#
# Globals Required:
#   An array called CHR_NAMES with the valid chromosomes
# Arguments:
#   $1: A comma separated string of chromosomes passed by the user
# Globals Set:
#   An array called process_chrs with the validated chromosomes
# Returns:
#   1 if the chromosomes could not be validated and 0 if they can
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
check_chr_names() {
    local chr_name_str="$1"

    if [[ "$chr_name_str" == "" ]] || [[ "$chr_name_str" =~ ^\s+$ ]]; then
        process_chrs=("${CHR_NAMES[@]}")
    else
        process_chrs=($(echo "$chr_name_str" | tr ',' ' '))
        for i in "${process_chrs[@]}"; do
            idx=$(get_index_zero "$i" "${CHR_NAMES[@]}")

            if [[ $idx -eq -1 ]]; then
                error_msg "unknown chromosome: $i"
                error_exit "$LINENO"
            fi
        done
    fi
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Get the sample identifiers from a BGEN file. This is suprisingly tricky to do
# and involves using bgenix to output a VCF and bcftools to get the sample row.
# So the bgen file needs to be bgenix indexed, i.e. it needs a .bgi file with
# the same base name and bgenix, sqlite3 and bcftools need to be in your path.
#
# Arguments:
#   $1: The bash to an indexed BGEN file.
# Globals Set:
#   An array called process_chrs with the validated chromosomes.
# Returns:
#   1 if the vcf conversion does not have the correct header.
# Outputs:
#   This will echo back the sample IDs that have been extracted from the bgen
#   file, in the order they have been extracted.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
get_bgen_samples() {
    local bgen_file="$1"
    local bgi_file="$bgen_file".bgi

    # Get a snp that we will query for all the samples
    local search_snp=$(sqlite3 "$bgi_file" "select rsid from variant limit 1")

    # Here is what is going on here:
    # 1. Use the search SNP to output a single row of genotypes (with sample
    #    IDs) in VCF format.
    # 2. Then extract the sample line from the VCF header
    # 3. Substitute the tabs in the VCF sampl line for newlines
    # 4. Then remove the first 9 columns from the sample header (the
    #    non-sample columns)
    # 5. Output the sample IDs with their position in the sample header
    # 6. Sort the file on the sample ID
    vcf_header=($(bgenix -g "$bgen_file" -incl-rsids "$search_snp" -vcf 2>/dev/null |
                      bcftools view -h |
                      tail -n1))
    lead_cols=(${vcf_header[@]:0:9})
    if [[ ${lead_cols[0]} != "#CHROM" ]] || [[ ${lead_cols[-1]} != "FORMAT" ]]; then
        error_msg "bad vcf export: $bgen_file"
        return 1
    fi

    # Echo out the sample sections from the header
    echo ${vcf_header[@]:9}
}
