# BASH Helpers
A collection of bash helper functions that can be sourced into your scripts. They are written with later bash versions in mind i.e. bash >= 4.0.

I am no bash guru and welcome any improvments. To install:

```
git clone git@gitlab.com:cfinan/bash-helpers.git
```

Then add `bash-helpers` to your `$PATH` in your `~/.bashrc` or `~/.bash_profile`, i.e.

```
PATH="/path/to/bash-helpers/lib:${PATH}"
export PATH
```

Currently there is no real documentation other than docstrings within the source code, these should enable ou to use them effectively. I will gradually sort that out. Another project that I find really handy in bash scripts is [`shflags`](https://github.com/kward/shflags) so if you are not familiar with it is well worth checking out.
