## Functions for performing some common greenplum operations
## For safety have `set -o pipefail`

## Stop a gpfdist process on a port
## the default port is 8081
stop_gpfdist() {
    local port=${1:-8081}
    
    local process=$(ps -aux | grep gpfdist | grep $port | sed 's/\s\+/\t/g' | cut -f2)

    if [[ ! -z $process ]] && [[ $process =~ ^[1-9][0-9]*$ ]]; then
	# echo "[info] killing process '$process'"
	kill $process
    fi
}

## Check for a gpfdist process running on port
## echo 1 if there is a process running 0 if not
## the default port is 8081
check_gpfdist() {
    local port=${1:-8081}
    
    local process=$(ps -aux | grep gpfdist | grep $port | sed 's/\s\+/\t/g' | cut -f2)

    if [[ ! -z $process ]] && [[ $process =~ ^[1-9][0-9]*$ ]]; then
	echo 1
    else
	echo 0
    fi
}
