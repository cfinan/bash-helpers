# `bashmisc` directory
This has miscillanious bash scripts and functions to source in it. The scripts in here should be added to your `$PATH`. This readme details the usage of the scripts.

## Files
### `setup_paths.sh`
This script creates two directories in the root of the code direcotry. These are `pypath` and `progpath`. It then sets up a load of symbolic links of my python modules into the `pypath` and and various programs to `progpath`. It is the users responsibility to add `progpath` to `$PATH` in the `~/.bashrc` and `pypath` to `$PYTHONPATH` in the `~/.bashrc`. The script requires the root to the code directory to be provided and this is a fatal error if it does not exist. Below is an example run

```
./setup_paths.sh "/home/rmjdcfi/code"
```

## Directories
* `shflags-1.0.3` - The shflags library see [here](https://github.com/kward/shflags). This actually needs updating. The latest version is ~1.2. The shflags library is located in `shflags-1.0.3/src`
