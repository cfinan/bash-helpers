################################################################################
# A set of utility functions that are designed to do common tasks in bash      #
# scripts                                                                      #
################################################################################
source "trap_stack.sh"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# Parse a SQL connection string into MySQL command line attributes             #
# $1: The SQL alchemy connection string                                        #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
parse_sql_conn() {
    local connect_str="$1"

    # A python script does the parsing and we capture and process the output
    local connatts=($(parse_sql_connection -v "$connect_str"))

    # Loop through all the connection attributes
    for i in $(seq 0 $((${#connatts[@]} -1))); do
	# We want to ignore the driver
	if [[ ${connatts[$i]} =~ "driver=" ]]; then
	    unset connatts[$i]
	    continue
	fi

	# Add the leading flag  to them
	connatts[$i]="--"${connatts[$i]}
    done

    # Echo the connection string back to be caught
    echo ${connatts[@]}
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# echo the database name from an SQL alchemy like connection string           #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
get_dbname() {
    local dbname=$(parse_sql_conn "$1" | perl -ne '$_ =~ m/--database=(\w+)\s+/; print $1')
    echo "$dbname"
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# removes the database name from a connection string and returns the          #
# attributes                                                                  #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
strip_dbname() {
    local dbname=$(parse_sql_conn "$1" | perl -ne '$_ =~ s/--database=\w+\s+/ /; print $_')
    echo "$dbname"
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# Get the index value of an element in an array, or return -1 if it is not     #
# present                                                                      #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
index () {
  local e
  local idx=0
  local found=0
  #  for e in "${@:2}"; do [[ "$e" == "$1" ]] && idx=$((idx+1)); break; done
  for e in "${@:2}"; do
      if [[ "$e" == "$1" ]]; then
	  found=$((found+1))
	  break
      fi
      idx=$((idx+1))
  done
  
  if [[ $found -eq 1 ]]; then
      echo $idx
  else
      echo -1
  fi
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# Get the index value of an element in an array, or return -1 if it is not     #
# present                                                                      #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
element_in () {
  local e
  for e in "${@:2}"; do
      if [[ "$e" == "$1" ]]; then
	  echo 0
	  return 0
      fi
  done

  echo 1
  return 1
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# A helper function that will mask the password from an SQL connection string  #
# so it can be echo'd to STDOUT safely                                         #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
clean_sql_pw () {
    local sql="$1"

    local clean_sql=$(echo "$sql" | sed -E 's/(:\/\/.+):.*?@/\1:XXXXX@/')
    echo "$clean_sql"
}


################################################################################
# Read secret string, can be used for password prompts                         #
# see: http://stackoverflow.com/questions/3980668/how-to-get-a-password-from-a-shell-script-without-echoing
# this looks potentially useful for handling traps:
# http://stackoverflow.com/questions/16115144/bash-save-and-restore-trap-state-easy-way-to-manage-multiple-handlers-for-trap
################################################################################
read_secret() {
    oldtty=$(stty -g)
    # Disable echo.
    stty -echo

    # Set up trap to ensure echo is enabled before exiting if the script
    # is terminated while echo is disabled.
    trap_append 'stty echo' EXIT ERR SIGINT SIGTERM SIGHUP

    # Read secret.
    read "$@"

    # Enable echo.
#q    stty echo
    stty $oldtty

    # Remove the stty echo from the signals
    trap_pop EXIT ERR SIGINT SIGTERM SIGHUP

    # Print a newline because the newline entered by the user after
    # entering the passcode is not echoed. This ensures that the
    # next line of output begins at a new line.
    echo
}

################################################################################
# Makes sure the last line in a file ends with a newline                       #
################################################################################
last_line() {
    # Note this does not work as it produces an exit code of 1 (if the file is 
    # ok) this activates the trap
    local filename="$1"
    local newline='
'
#    echo "$filename"
    lastline=$(tail -n1 "$filename"; echo x); 
#    echo "$lastline"
    lastline=${lastline%x}
#    echo "$lastline"

    # We have to remove any error traps before we do this as it generates 
    # an error code, so:
    # Back up any current error traps
    trap_bak ERR

    # Remove any current error traps
    trap - ERR
    [ "${lastline#"${lastline%?}"}" != "$newline" ] && echo >> "$filename"

    # reinstate the error traps from the backup
    # quoting on the array is important as the trap commands may have spaces
    for i in "${trap_stack_ERR_bak[@]}"; do
	trap_append "$i" ERR
    done
#    trap 'error ${LINENO}' ERR

}

################################################################################
# A small function to echo out a message if verbose is TRUE. The message is    #
# echoed to STDOUT. Note that VERBOSE should be defined somewhere in the script#
# that is calling this.                                                        #
# message: The message to echo                                                 #
# prefix: An optional prefix to the message                                    #
################################################################################
msg() {
    local message="$1"
    local prefix="${2:-""}"
    
    # This will respond to verbose TRUE or 1
    if [[ $VERBOSE == true ]] || [[ $VERBOSE -eq 0 ]]; then
	echo "$prefix""$message"
    fi
}


################################################################################
# See http://stackoverflow.com/questions/64786/error-handling-in-bash          #
# teh tempfiles variable will hold temp file paths that have to be cleaned up  #
# when the script exist                                                        #
################################################################################
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Temp files are files that are cleaned when the script exits with an error or #
# when it exits cleanly                                                        #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
tempfiles=( )
cleanup_temp() {
    # Note that this will remove directories as well, the -f (force flage will
    # stop errors being generated if the files/dirs do not exist
    if [[ ${#tempfiles[@]} -gt 0 ]]; then
       rm -rf "${tempfiles[@]}"
    fi
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Error files are files that are cleaned when the script exits with an error   #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
errorfiles=( )
cleanup_error() {
    # Note that this will remove directories as well, the -f (force flage will
    # stop errors being generated if the files/dirs do not exist
    if [[ ${#errorfiles[@]} -gt 0 ]]; then
       rm -rf "${errorfiles[@]}"
    fi
}

################################################################################
# trap bash errors                                                             #
################################################################################
error() {
  local parent_lineno="$1"
  local message="$2"
  local code="${3:-1}"
  if [[ -n "$message" ]] ; then
    echo "Error on or near line ${parent_lineno}: ${message}; exiting with status ${code}"
  else
    echo "Error on or near line ${parent_lineno}; exiting with status ${code}"
  fi

  # Now do the cleanup
  cleanup_temp
  cleanup_error

  # Finally exit with the error code
  exit "${code}"
}

################################################################################
# Initialise trapping errors                                                   #
################################################################################
trap_errors_on() {
    # This turns on the trap errors and other
    # termination signals 
    trap 'error ${LINENO}' ERR SIGHUP SIGINT SIGTERM
}

################################################################################
# turn off trapping errors                                                     #
# Note that this does not work when placed in a function!?!?!                  #
################################################################################
trap_errors_off() {
    #
    # trap - ERR
    trap - ERR SIGHUP SIGINT SIGTERM
    # trap - ERR SIGHUP SIGINT SIGTERM
}

################################################################################
# Initialise trapping errors                                                   #
################################################################################
trap_exit_on() {
    # This turns on the trap errors and other
    # termination signals 
    trap cleanup_temp EXIT
}

################################################################################
# Initialise trapping errors                                                   #
################################################################################
trap_exit_off() {
    # This turns on the trap errors and other
    # termination signals 
    trap - EXIT
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# Test if a file is compressed and echo 0 if yes or 1 if no                    #
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
is_compressed() {
    local infile="$1"
    local test_compressed=$(file "$infile")
    local compressed=1

    if [[ "$test_compressed" =~ "gzip compressed" ]]; then
	compressed=0
    fi

    echo $compressed
}

################################################################################
# Check if a directory is writable by a given UID                              #
# will create a variable called ACCESS which will either have the value yes or #
# no                                                                           #########################
# see: http://stackoverflow.com/questions/14103806/bash-test-if-a-directory-is-writable-by-a-given-uid #
########################################################################################################
dir_writable() {
    local DIR="$1"

    if [[ -z $DIR ]]; then
        echo "[FATAL] No directory to test" 1>&2
        exit 1
    fi
    
    # Use -L to get information about the target of a symlink,
    # not the link itself, as pointed out in the comments
    INFO=( $(stat -L -c "%a %G %U" "$DIR") )
#    echo $?
#    echo ${INFO[@]}
    PERM=${INFO[0]}
    GROUP=${INFO[1]}
    OWNER=${INFO[2]}

    ACCESS=no
    if [[ $PERM && 0002 != 0 ]]; then
        # Everyone has write access
        ACCESS=yes
    elif [[ $PERM && 0020 != 0 ]]; then
        # Some group has write access.
        # Is user in that group?
        gs=( $(groups $USER) )
        for g in "${gs[@]}"; do
            if [[ $GROUP == $g ]]; then
                ACCESS=yes
                break
            fi
        done
    elif [[ $PERM && 0200 != 0 ]]; then 
        # The owner has write access.
        # Does the user own the file?
        [[ $USER == $OWNER ]] && ACCESS=yes
    fi
}


################################################################################
# Given a file name it will attempt to detect the delimiter it tests:          #
# \t,\s,",",";"                                                                #
# *** NOTE THIS DOES NOT WORK PROPERLY AT THE MOMENT ***                       #
################################################################################
detect_delimiter() {
	local FILE="$1"
	local N_LINE="$2"
	
	# Set the default number of lines
	if [[ -z ${N_LINE} ]]; then
		local N_LINE=10
	fi
	
#	IFS='%'
	TAB='\t'
	SPACE='\s'
	COMMA=","
	SEMI=';'
	local DELIMITERS=("${TAB}" "${SPACE}" "${COMMA}" "${SEMI}")
	# I will look for the delimiter in the first N lines
	for d in ${DELIMITERS}; do
		echo DELIMITER="${d}"
		# If the file is a gzipped file
		if [[ ${FILE} =~ .gz$ ]]; then
			LINES=($(zcat "${FILE}" | head -n${N_LINE} | cct -d$"${d}" | tr "," " "))
		else	
			LINES=($(cat "${FILE}" | head -n${N_LINE} | cct -d$"${d}" | tr "," " "))
		fi
		echo ${LINES[@]}
	done
#	unset IFS
}


##########################################################################
# print the header (the first line of input)                             #
# and then run the specified command on the body (the rest of the input) #
# use it in a pipeline, e.g. ps | body grep somepattern                  #
##########################################################################
body() {
    IFS= read -r header
    printf '%s\n' "$header"
    "$@"
}


vcfbody() {
    
    while true; do
	IFS= read -r header

	if [[ "$header" =~ ^## ]]; then
	    continue
	elif [[ "$header" =~ ^#CHROM ]]; then
	    printf '%s\n' "$header"
	    break
	else
	    echo "not a vcf!" 1>&2
	fi		 
    done
    
    "$@"
}


